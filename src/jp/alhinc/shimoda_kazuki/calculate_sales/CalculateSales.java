package jp.alhinc.shimoda_kazuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		Map<String, String> branchNameMap = new HashMap<String, String>();//支店情報マップ宣言
		Map<String, Long> salesMap = new HashMap<String, Long>();//売上情報マップ宣言


		//処理①
		BufferedReader br = null; // BufferedReaderの初期化

		try {
			File file = new File(args[0], "branch.lst");
			//ファイルを指定

			FileReader fr = new FileReader(file);//指定したファイルをfileReaderに設置
			br = new BufferedReader(fr); //FileReaderのファイルをBuffereReaderに設置

			String line;//変数作成
			while((line = br.readLine()) != null) {
				//変数lineに「readLine」で1行ずつファイルを読み込み

				//エラー処理①-2 フォーマット不正
				if(line.matches("[0-9]{3},.*") == false) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				String[] branchCode = line.split(",");
				//spritメゾットで支店コードを[0],支店名を[1]に分けた
				if(branchCode.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				branchNameMap.put(branchCode[0], branchCode[1]);
				//mapに支店コードと支店名を追加
				salesMap.put(branchCode[0], (long) 0);
				//mapに支店コードと売上を追加
			}
		}catch(FileNotFoundException fnfe) {
			System.out.println("支店定義ファイルが存在しません");
			return;
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		//処理②
		File directory = new File(args[0]);//directoryにファイルの位置を指定
		FilenameFilter filter = new FilenameFilter() { //フィルタ作成

			public boolean accept(File file, String str) {
				if (str.matches("[0-9]{8}.rcd")) {
					return true;
				}else {
					return false;     // 「8桁の数字」と「rcd」がついるものだけを
				}
			}
		};

		File[] fileName = directory.listFiles(filter);//配列fileName作成
		//その中に上で作ったフィルターにかけたファイル一覧を取得
		for(int i = 0; i < fileName.length; ++i) {

			//エラー処理②-2 連番
			String salesData = fileName[i].getName(); //getNameでファイル名取得
			String salesName = salesData.substring(0, 8); //substringで8桁の数字だけ取得
			int salesNumber = Integer.parseInt(salesName);//int型にキャスト

			String minimumData = fileName[0].getName();
			String minimumName = minimumData.substring(0, 8);
			int minimumNumber = Integer.parseInt(minimumName);//配列の最小値をint型で取得

			if(salesNumber != i + minimumNumber) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

			try {
				FileReader fr = new FileReader(fileName[i]);//指定したファイルをFileReaderに設置
				br = new BufferedReader(fr);//FileReaderのファイルをBuffereReaderに設置

				String line;
				List<String> list = new ArrayList<String>();
				while((line = br.readLine()) != null) {
					list.add(line);
				}
				if(list.size() != 2) {
					System.out.println(salesData + "のフォーマットが不正です");
					return;
				}

				String branchNumber = list.get(0);
				Long salesCode = Long.parseLong(list.get(1));
				Long salesCode2 = salesMap.get(branchNumber);
				//変数saleCode2にmap

				//エラー処理②-3 支店コードに該当なし
				if(salesMap.containsKey(branchNumber) == false) {
					System.out.println(salesData + "の支店コードが不正です");
					return;
				}

				Long salesTotal = salesCode + salesCode2;
				//変数saleTotalに加算した合計を設置

				//エラー処理②-2 合計が10桁を超えた場合
				if(salesTotal >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				salesMap.put(branchNumber, salesTotal);
				//saleInfoに上書きして追加
			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//処理③
		File file = new File(args[0], "branch.out");//ファイルの位置を指定
		BufferedWriter bw = null;

		try {
			bw = new BufferedWriter(new FileWriter(file));
			for(Map.Entry<String, String> entry : branchNameMap.entrySet()) { //mapの値(キーとバリューどちらも)を取得

				String branchCode = entry.getKey();//storeCodeに店番を取得
				String branchName = entry.getValue();//storeNameに店名を取得
				Long salesTotal = salesMap.get(branchCode);
				//saleTotalにsaleInfoマップから金額(加算)を取得

				bw.write(branchCode + "," + branchName + "," + salesTotal);
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました"); //エラー処理③
			return;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}
}
